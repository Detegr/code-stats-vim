" check Python 2 or 3 support
let s:codestats_autoload_path = fnamemodify(resolve(expand('<sfile>:p')), ':h')
if has('python3')
    execute 'py3file ' . s:codestats_autoload_path . '/../python/codestats.py'
    let s:python = 'python3'
elseif has('python')
    execute 'pyfile ' . s:codestats_autoload_path . '/../python/codestats.py'
    let s:python = 'python'
else
    echomsg 'code-stats-vim requires Python support!'
    finish
endif

" Check InsertCharPre support (Vim >= 7.3.186 in practice)
if !exists('##InsertCharPre')
    echomsg 'code-stats-vim requires InsertCharPre support (Vim >= 7.3.186)!'
    finish
endif

" API key: required
if !exists('g:codestats_api_key')
    echomsg 'code-stats-vim requires g:codestats_api_key to be set!'
    finish
endif

" API endpoint
if !exists('g:codestats_api_url')
    let g:codestats_api_url = 'https://codestats.net'
endif

" Two XP counters
if !exists("g:codestats_pending_xp")
  let g:codestats_pending_xp = 0 " global total of unsaved XP
endif

if !exists("b:codestats_xp")
  let b:codestats_xp = 0 " buffer-local XP
endif

function! codestats#add_xp()
    let g:codestats_pending_xp += 1
    let b:codestats_xp += 1
endfunction

function! codestats#log_xp()
    execute s:python . ' log_xp()'
endfunction

function! codestats#exit()
    execute s:python . ' stop_worker()'
endfunction

" Export function that returns pending xp like "C::S 13"
function! codestats#CodeStatsXp()
    return 'C::S ' . g:codestats_pending_xp
endfunction

function codestats#CodestatsCheckXp(timer_id)
    execute s:python . ' check_xp()'
endfunction
