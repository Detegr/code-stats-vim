" Handle Vim events
augroup codestats
    autocmd!

    " STARTUP
    autocmd BufEnter * if !exists('b:codestats_xp') | let b:codestats_xp = 0 | endif

    " ADDING XP: Insert mode
    " Does not fire for newlines or backspaces,
    " TextChangedI could be used instead but some
    " plugins are doing something weird with it that
    " messes up the results.
    autocmd InsertCharPre * call codestats#add_xp()

    " ADDING XP: Normal mode changes
    autocmd TextChanged * call codestats#add_xp()

    " LOGGING XP
    autocmd InsertEnter,InsertLeave,BufEnter,BufLeave * call codestats#log_xp()

    " STOPPING
    autocmd VimLeavePre * call codestats#exit()
augroup END

" check xp periodically if possible
if has('timers')
    " run every 500ms, repeat infinitely
    let s:timer = timer_start(500, 'codestats#CodestatsCheckXp', {'repeat': -1})
endif

command! CodeStatsXp echo codestats#CodeStatsXp()
